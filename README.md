# doctor-everywhere-reference-app

# To Run the app using Docker Compose
* Build the app by running Docker build commands. 
  * ```docker build -f doctor-api.Dockerfile -t doctor-api:latest .```
  * ```docker build -f patient-api.Dockerfile -t patient-api:latest .``` 
  * ```docker build -f appointments.Dockerfile -t appointments-api:latest .```
    
or

just run below command if ur on mac or linux
  * ```sh appBuild.sh```
   
* Run Docker Compose Up: ```docker-compose up```

### Spring Security Impl
1. Add Spring Security Starter Pack in POM
2. Add Security Config File, annotate it with @Configuration and @EnableWebSecurity Class

### Run Ui App
* npm install
* npm run dev

#### Binaries Installation
* Docker
* AWS cli: https://aws.amazon.com/cli/
* Kubectl : https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/
* eksctl: https://eksctl.io/installation/
* aws-iam-authenticator: https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html

* Add all the binaries in a fodler and then add that path in your environment variable path.
* Create a .kube folder in your users drive example: C:/Users/Akash/.kube
* Create a Config file in this folder.